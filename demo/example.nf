nextflow.enable.dsl=2

process RunBash {
    output:
    path("*")

    "echo inside main branch | tee out.txt"
}

workflow {
    RunBash() | view
    println "Done!"
}
